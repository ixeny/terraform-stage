variable "aws_access_key_id" {}
variable "aws_secret_access_key" {}
#variable "aws_session_token" {}

#Paris
variable "region" {
  default = "eu-west-3"
}

variable "amis" {
  default = {
    eu-west-3 = "ami-0451ae4fd8dd178f7"
}
}

#variable "vpc_id" {
#  default = "vpc-xxxxxxx"
#}
