###################### TERRAFORM - TOPOLOGIE DE TEST ###########################

################################################################################
#         Config credentials et region
################################################################################

provider "aws" {
  access_key = "${var.aws_access_key_id}"
  secret_key = "${var.aws_secret_access_key}"
#  token      = "${var.aws_session_token}"
  region     = "${var.region}"
}

################################################################################
#          VPC
################################################################################

resource "aws_vpc" "theVPC" {
  cidr_block = "172.31.0.0/16"
}

################################################################################
#          Internet gateway
################################################################################

resource "aws_internet_gateway" "igw" {
  vpc_id = "${aws_vpc.theVPC.id}"

  tags = {
    Name = "main igw"
  }
}

################################################################################
#          Creation des subnets (AZ a)
################################################################################

resource "aws_subnet" "subnet1" {
  vpc_id            = "${aws_vpc.theVPC.id}"
  availability_zone = "${var.region}a"
  cidr_block        = "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 1)}"
}

resource "aws_subnet" "subnet2" {
  vpc_id            = "${aws_vpc.theVPC.id}"
  availability_zone = "${var.region}a"
  cidr_block        = "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 2)}"
}

resource "aws_subnet" "subnet3" {
  vpc_id            = "${aws_vpc.theVPC.id}"
  availability_zone = "${var.region}a"
  cidr_block        = "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 3)}"
}

################################################################################
#          Subnets
################################################################################

resource "aws_subnet" "subnet4" {
  vpc_id            = "${aws_vpc.theVPC.id}"
  availability_zone = "${var.region}c"
  cidr_block        = "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 4)}"
}

resource "aws_subnet" "subnet5" {
  vpc_id            = "${aws_vpc.theVPC.id}"
  availability_zone = "${var.region}c"
  cidr_block        = "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 8)}"
}

resource "aws_subnet" "subnet6" {
  vpc_id            = "${aws_vpc.theVPC.id}"
  availability_zone = "${var.region}c"
  cidr_block        = "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 12)}"
}

################################################################################
#          EIP Addresses
################################################################################

resource "aws_eip" "eip1" {
  vpc = true
}

resource "aws_eip" "eip2" {
  vpc = true
}

################################################################################
#       NAT Gateways
################################################################################

resource "aws_nat_gateway" "nat_aza" {
  depends_on = ["aws_internet_gateway.igw"]
  subnet_id = "${aws_subnet.subnet1.id}"
  allocation_id = "${aws_eip.eip1.id}"

  tags = {
    Name = "NAT gateway 1"
  }
}

resource "aws_nat_gateway" "nat_azc" {
  depends_on = ["aws_internet_gateway.igw"]
  subnet_id = "${aws_subnet.subnet4.id}"
  allocation_id = "${aws_eip.eip2.id}"

  tags = {
    Name = "NAT gateway 2"
  }
}

################################################################################
#       Routing tables
################################################################################

resource "aws_route_table" "rt1" {
  vpc_id = "${aws_vpc.theVPC.id}"

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.igw.id}"
  }

  tags = {
    Name = "main"
  }
}

resource "aws_route_table" "rt2" {
  vpc_id = "${aws_vpc.theVPC.id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat_aza.id}"
  }

  tags = {
    Name = "AZ-a Private"
  }
}

resource "aws_route_table" "rt3" {
  vpc_id = "${aws_vpc.theVPC.id}"

  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = "${aws_nat_gateway.nat_azc.id}"
  }

  tags = {
    Name = "AZ-c Private"
  }
}

resource "aws_route_table" "rt4" {
  vpc_id = "${aws_vpc.theVPC.id}"
  tags = {
    Name = "AZ-a Data"
  }
}

resource "aws_route_table" "rt5" {
  vpc_id = "${aws_vpc.theVPC.id}"
  tags = {
    Name = "AZ-c Data"
  }
}

################################################################################
#       Routing tables subnets' associations
################################################################################

resource "aws_route_table_association" "associate_public1" {
  subnet_id      = "${aws_subnet.subnet1.id}"
  route_table_id = "${aws_route_table.rt1.id}"
}

resource "aws_route_table_association" "associate_public2" {
  subnet_id      = "${aws_subnet.subnet4.id}"
  route_table_id = "${aws_route_table.rt1.id}"
}

resource "aws_route_table_association" "associate_private_aza" {
  subnet_id      = "${aws_subnet.subnet2.id}"
  route_table_id = "${aws_route_table.rt2.id}"
}

resource "aws_route_table_association" "associate_private_azc" {
  subnet_id      = "${aws_subnet.subnet5.id}"
  route_table_id = "${aws_route_table.rt3.id}"
}

resource "aws_route_table_association" "associate_private_data_aza" {
  subnet_id      = "${aws_subnet.subnet3.id}"
  route_table_id = "${aws_route_table.rt4.id}"
}

resource "aws_route_table_association" "associate_private_data_azc" {
  subnet_id      = "${aws_subnet.subnet6.id}"
  route_table_id = "${aws_route_table.rt5.id}"
}

################################################################################
#          DB Subnet Group
################################################################################

resource "aws_db_subnet_group" "db_subnet_grp" {
  name       = "main"
  subnet_ids = ["${aws_subnet.subnet6.id}", "${aws_subnet.subnet3.id}"]

  tags = {
    Name = "My DB subnet group"
  }
}

################################################################################
#          RDS Database
################################################################################

resource "aws_db_instance" "rdsinstance" {
  allocated_storage    = 20
  storage_type         = "gp2"
  engine               = "mysql"
  engine_version       = "5.7"
  instance_class       = "db.t2.micro"
  name                 = "student_database"
  username             = "dbuser"
  password             = "password"
  availability_zone    = "${var.region}a"
  parameter_group_name = "default.mysql5.7"
  db_subnet_group_name = "${aws_db_subnet_group.db_subnet_grp.name}"
  skip_final_snapshot  = true
}

################################################################################
#          EC2 instance
################################################################################

resource "aws_security_group" "app" {
  name        = "terraform_example_elb"
  description = "Used in the terraform"
  vpc_id      = "${aws_vpc.theVPC.id}"

  # SSH access from anywhere
ingress {
  from_port   = 22
  to_port     = 22
  protocol    = "tcp"
  cidr_blocks = ["${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 8)}", "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 2)}"]
}

# HTTP access from the VPC
ingress {
  from_port   = 80
  to_port     = 80
  protocol    = "tcp"
  cidr_blocks = ["${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 8)}", "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 2)}"]
}

# HTTPS access from the VPC
ingress {
  from_port   = 443
  to_port     = 443
  protocol    = "tcp"
  cidr_blocks = ["${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 8)}", "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 2)}"]
}

  # outbound internet access
  egress {
    from_port   = 3306
    to_port     = 3306
    protocol    = "tcp"
    cidr_blocks = ["${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 12)}", "${cidrsubnet(aws_vpc.theVPC.cidr_block, 4, 3)}"]
  }
}
